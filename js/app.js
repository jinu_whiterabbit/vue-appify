(function(){

  var helpers = {

    /**
     * @description function to encode image as base64
     * @param {Object} event 
     * @return {Undefined}
     */
//     updateSplashBg : function(currColor){
//       $('.splash-bg').css('background', currColor);
// -    }, 
    imgUpload: function(event ) {
      var input = event.target;
      // check image size
      var size = $( input ).attr("data-size");
      if( size ){
        // split width and height
        size = size.split(",").map(function(item){
          return parseInt( item );
        });
        console.log( size );
      }
      if (input.files && input.files[0]) {
        // get filename
        var file = input.files[ 0 ] ;
        // split filename to get the extension
        var extension = ( file.name.split(".").pop() );
        // set allowed extensions
        var allowed = ["jpg","png","jpeg"];
        if(allowed.indexOf( extension) === -1 ){
          // alert if user uploaded inavlid file type
          alert("Not allowed");
        }
        $(input).parents('.tab-wrapper').eq(0).find('.home-screen-ios').addClass('has-app');
        // create a new FileReader to read this image and convert to base64 format
        var reader = new FileReader();
        // Define a callback function to run, when FileReader finishes its job
        reader.onload = (e) => {
           
            // Note: arrow function used here, so that "this.imageData" refers to the imageData of Vue component
            // Read image as base64 and set to imageData
            this.imageData = e.target.result;
            var image = new Image();
            image.onload = function(){
              // check file size
              if(this.width!== size[ 0 ] || this.height!==size[1] ){
                // alert if user uploaded invalid file size
                alert( "Invalid size , please upload recommended image size "+(size.join("x")));
              }
            }
            image.src = this.imageData ;
        }
        // Start the reader job - read file as a data url (base64 format)
        reader.readAsDataURL(input.files[0]);
      }
    }
  };
 
  // initialize the Vue app
  new Vue({
    el: '#main',
    // configure and initialize all app variables that will be used such as activetab, appname, splashname, imageData etc.
    data: { 
      activetab: 1,
      appname: '',
      splashname: '',
      imageData: '',
      checkSpalsh: false,
      currColor: '',
      logoMaxSize: '1',
      splashMaxSize: ''
    },
    methods: {
      onSubmit(e) {
        // alert('123');
      },
      // Call imgUpload function to preview uploaded image
      previewImage: function(event) {
        // console.log(event.target.files);
         helpers.imgUpload.call(this,event)
      },
      // set background color to splash screen and color to the text
      selectColor: function(event) {
        // Get data-color attribute and assigned the color to a variable
        let currColor = $(event.target).attr('data-color'); 
        // if item has "splashBackground" class, set background color
        if(event.target.classList.contains('splashBackground')) {
          $('.splash-bg').css('background', currColor);
        }
        else {
          // set color to the text
          $('.app-name-splash').css('color', currColor);
        }
      },
      // update background color from color picker
      colorPicker: function(event) {
        var currColor = event.target.value;
        $('.splash-bg').css('background', currColor);
        // helpers.updateSplashBg(event.target.value);
      },
  },
  
  });
})();